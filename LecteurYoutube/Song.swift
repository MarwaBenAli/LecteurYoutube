//
//  Song.swift
//  LecteurYoutube
//
//  Created by haithem elloumi on 31/08/2018.
//  Copyright © 2018 test company. All rights reserved.
//

import Foundation

class Song {
    
    private var _artist : String
    private var _title : String
    private var _code : String
    private var _baseUrlVideo = "https://www.youtube.com/embed/"
    private var _baseUrlMineature = "http://i.ytimg.com/vi/"
    private var _endUrlMineature = "/maxresdefault.jpg"
    
    
    var artiste : String {
        return _artist
    }
    
    var title : String {
        return _title
    }
    
    var videoUrl : String {
        return _baseUrlVideo + _code
    }
    
    var mineatureUrl : String {
        return _baseUrlMineature + _code + _endUrlMineature
    }
    
    init(artiste: String, title: String, code: String){
        self._artist = artiste
        self._title = title
        self._code = code
    }
}
