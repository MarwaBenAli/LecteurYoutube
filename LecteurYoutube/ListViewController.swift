//
//  ListViewController.swift
//  LecteurYoutube
//
//  Created by haithem elloumi on 31/08/2018.
//  Copyright © 2018 test company. All rights reserved.
//

import UIKit

class ListViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var listView: UITableView!
    var songs = [Song]()
    let identifCell = "songcell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listView.delegate = self
        listView.dataSource = self
        addSong()
        title = "Mes vidéos préférées"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let song = songs[indexPath.row]
        if let cell = listView.dequeueReusableCell(withIdentifier: identifCell) as? SongCell{
        cell.createCell(song: song)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func addSong(){
        songs = [Song]()
        let cool = Song(artiste: "Kendji",title: "Andalouse",code: "2bjk26RwjyU")
        songs.append(cool)
        let cool1 = Song(artiste: "Adelo",title: "hELLO",code: "PejyoeG_TmA")
        songs.append(cool1)
        let cool2 = Song(artiste: "CELINE DION",title: "Je t'aime",code: "cz-PLtXh7Lo")
        songs.append(cool2)
        listView.reloadData()
    }
}
