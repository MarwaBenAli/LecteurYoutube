//
//  SongCell.swift
//  LecteurYoutube
//
//  Created by haithem elloumi on 31/08/2018.
//  Copyright © 2018 test company. All rights reserved.
//

import UIKit

class SongCell: UITableViewCell {
    
    @IBOutlet weak var miniature: UIImageView!
    @IBOutlet weak var artistTitleLabel: UILabel!
    
    var song: Song!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func createCell(song: Song){
        self.song = song
        downloadImage()
        let attributed = NSMutableAttributedString(string: self.song.title, attributes: [.font: UIFont.boldSystemFont(ofSize: 20), .foregroundColor: UIColor.black])
        let secondeLigne = NSAttributedString(string: "\n" + self.song.artiste, attributes: [.font: UIFont.italicSystemFont(ofSize: 20), .foregroundColor: UIColor.darkGray])
        attributed.append(secondeLigne)
        artistTitleLabel.attributedText = attributed
    }
    
    func downloadImage() {
        miniature.image = #imageLiteral(resourceName: "logo")
        
        if let url = URL(string: self.song.mineatureUrl) {
            let session = URLSession.shared
            let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
                if let imageData = data, let image = UIImage(data: imageData) {
                    DispatchQueue.main.async {
                        self.miniature.image = image
                    }
                }
            })
            task.resume()
            
        }
    }
    
}
